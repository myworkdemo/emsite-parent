/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.cms.entity.Category;
import com.empire.emsite.modules.cms.facade.CategoryFacadeService;
import com.empire.emsite.modules.cms.service.CategoryService;
import com.empire.emsite.modules.sys.entity.User;

/**
 * 类CategoryFacadeServiceImpl.java的实现描述：栏目FacadeService接口实现类
 * 
 * @author arron 2017年9月17日 下午9:45:05
 */
@Service
public class CategoryFacadeServiceImpl implements CategoryFacadeService {
    @Autowired
    private CategoryService categoryService;

    @Override
    public List<Category> findByUser(boolean isCurrentSite, String module, User user, String currSiteId) {
        return categoryService.findByUser(isCurrentSite, module, user, currSiteId);
    }

    @Override
    public List<Category> findByParentId(String parentId, String siteId) {
        return categoryService.findByParentId(parentId, siteId);
    }

    @Override
    public Page<Category> find(Page<Category> page, Category category) {
        return categoryService.find(page, category);
    }

    @Override
    public void save(Category category) {
        categoryService.save(category);
    }

    @Override
    public void delete(Category category) {
        categoryService.delete(category);
    }

    /**
     * 通过编号获取栏目列表
     */
    @Override
    public List<Category> findByIds(String ids) {
        return categoryService.findByIds(ids);
    }

    @Override
    public Category get(String id) {
        return categoryService.get(id);
    }

}
