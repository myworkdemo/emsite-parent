/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.dao;

import com.empire.emsite.common.persistence.CrudDao;
import com.empire.emsite.common.persistence.annotation.MyBatisDao;
import com.empire.emsite.modules.cms.entity.Guestbook;

/**
 * 类GuestbookDao.java的实现描述：留言DAO接口
 * 
 * @author arron 2017年10月30日 下午4:07:54
 */
@MyBatisDao
public interface GuestbookDao extends CrudDao<Guestbook> {

}
