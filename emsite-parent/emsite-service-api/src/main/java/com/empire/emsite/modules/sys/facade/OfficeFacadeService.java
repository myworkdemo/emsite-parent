/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.sys.facade;

import java.util.List;

import com.empire.emsite.modules.sys.entity.Office;
import com.empire.emsite.modules.sys.entity.User;

/**
 * 类OfficeFacadeService.java的实现描述：机构FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:52:26
 */
public interface OfficeFacadeService {
    public Office get(String id);

    public List<Office> findAll(User user);

    public List<Office> findList(Boolean isAll, User user);

    public List<Office> findList(Office office);

    public void save(Office office);

    public void delete(Office office);

    public List<Office> findAllList();
}
